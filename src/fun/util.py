
# function that always returns a contant
def Const(a):
    def f():
        return a
    return f
